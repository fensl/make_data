# make_data

#### 介绍
信奥oj需要随机生成数据，肝了这个项目，方便生产评测数据。

#### 软件架构
linux+python+guizero


#### 安装教程

1.  克隆
2.  pip install guizero

#### 使用说明

1. 运行`make_data.py`文件
2. 左上角选择数据保存位置
3. 右上角配置变量和数据模型 
4. 填写cpp的答案
5. 点击保存数据

![](imgs/1.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 更新日志

**2022.10.15**
初步实现：
1. 根据数据格式要求，生成.in文件
2. 根据用户填写的cpp答案，生成.out文件