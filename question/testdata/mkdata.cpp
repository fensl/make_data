#include <bits/stdc++.h>
#define endl '\n'
using namespace std;

int a = 1;
int b = 10;

void test(ofstream &fout)
{
    int n = rand() % 10000;
    fout << n << endl;
    for (int i = 0; i < n; i++)
    {
        fout << rand() % 2000 << " " << rand() % 100 << endl;
    }
}

void mk_in()
{
    srand(time(NULL));
    for (int i = a; i <= b; ++i)
    {
        string in_name = to_string(i) + ".in";
        ofstream fout(in_name);
        test(fout); // 生成测试数据
        cout << "生成【" << i << ".in】数据成功\n";
    }
}

void mk_out()
{
    system("g++ std.cpp -o std -std=c++14 -O2 -fmax-errors=1"); // 编译标程
    for (int i = a; i <= b; ++i)
    {
        string in_name = to_string(i) + ".in";
        string out_name = to_string(i) + ".out";
        string cmd = "./std < " + in_name + " > " + out_name; // 组合文件重定向的命令
        system(cmd.c_str());                                  // 运行程序
        printf("生成【%d.out】数据成功\n", i);
    }
}

int main()
{
    mk_in();
    mk_out();
    return 0;
}