#include <iostream>
using namespace std;

// 已知
int n, need[10000], dist[10000];
// 未知
long long l = 0;
int pos = 0;
long long cost = 0;
void input()
{
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> need[i] >> dist[i];
        l += dist[i];
    }
}
void solve()
{
    for (int i = 0; i < n; ++i)
    {
        long long len = 0, now_cost = 0;
        for (int j = i; j < n + i; ++j)
        {
            int real_len = len < l / 2 ? len : l - len;
            now_cost += need[j % n] * real_len;
            len += dist[j % n]; // 过了这个点，距离增加
        }
        if (now_cost < cost || cost == 0)
        {
            cost = now_cost;
            pos = i;
        }
    }
}
int main()
{
    input();
    solve();
    cout << pos << ',' << cost;
    return 0;
}