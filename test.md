测试ppt
<!-- slide -->

# 这是ppT的第一页
我随便写点啥
<!-- slide -->

# 这是第二页

```cpp{.line-numbers}
# include<iostream>
using namespace std;

int main()
{
    cout<<"hello world!"
    cout<<"hello world!"
}
```
![alt text](imgs/img.png)

<!-- slide -->

第三页幻灯片在这
ss
s


# 动态规划的应用
## 1. 0-1背包问题
### 1.1 问题描述
有n件物品和一个容量为c的背包，每件物品的体积为v[i]，价值为w[i]，求解将哪些物品装入背包可使这些物品的总体积不超过背包容量，且总价值最大。
### 1.2 状态定义
设dp[i][j]表示在前i件物品中选择若干件放入容量为j的背包中所能获得的最大价值。
### 1.3 状态转移方程
dp[i][j] = max(dp[i-1][j], dp[i-1][j-v[i]] + w[i])
### 1.4 初始条件
dp[0][j] = 0, dp[i][0] = 0
### 1.5 空间优化
由于dp[i][j]只与dp[i-1][j]和dp[i-1][j-v[i]]有关，因此可以使用一维数组进行空间优化。
```cpp
for(int i = 1; i <= n; i++) {
    for(int j = c; j >= v[i]; j--) {
        dp[j] = max(dp[j], dp[j-v[i]] + w[i]);
    }
}
```
## 2. 最长公共子序列问题
### 2.1 问题描述
给定两个字符串s1和s2，求它们的最长公共子序列的长度。
### 2.2 状态定义
设dp[i][j]表示s1的前i个字符和s2的前j个字符的最长公共子序列的长度。
### 2.3 状态转移方程
dp[i][j] = dp[i-1][j-1] + 1, if s1[i] == s2[j]
dp[i][j] = max(dp[i-1][j], dp[i][j-1]), if s1[i] != s2[j]
### 2.4 初始条件
dp[0][j] = 0, dp[i][0] = 0
### 2.5 空间优化
由于dp[i][j]只与dp[i-1][j]和dp[i][j-1]有关，因此可以使用一维数组进行空间优化。
```cpp
for(int i = 1; i <= n; i++) {
    for(int j = 1; j <= m; j++) {
        if(s1[i-1] == s2[j-1]) {
            dp[j] = dp[j-1] + 1;
        } else {
            dp[j] = max(dp[j], dp[j-1]);
        }
    }
}

#include <iostream>
using namespace std;

// 已知

// 未知


int main()
{
    
    return 0;
}

freopen("1.in","r",stdin);
freopen("1.out","w",stdout);

#pragma endregion


<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->
<!-- slide -->

<!-- slide -->

<!-- slide -->

