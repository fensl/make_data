import os
import random

N=10 # 数量

path=os.path.abspath(__file__)
path=os.path.dirname(path)
if not os.path.isdir(os.path.join(path,"test")):
    os.mkdir(os.path.join(path,"test"))
for i in range(N):
    filepath=os.path.join(path,"test",f"test-{i}.txt")
    with open(filepath,"w") as f:
        f.write(f"{random.random()}")
    print(f"创建文件：{filepath}成功")