/*
这题一眼递归，提示都很明确了，让你递归画三个小三角形
难点是找规律，和图形的表示
我们直接用矩阵来存放这个分形图
然后以三角形的顶点坐标和层数为状态参数，递归他即可
这次我们都从1开始数数，坐标好算
*/
#include<iostream>
using namespace std;
// 已知
int n;

// 未知
char matrix[1<<11][1<<11];

void dfs(int y,int x,int k)
{
    if(k==1)
    {
        matrix[y][x]='/';
        matrix[y][x+1]='\\';
        matrix[y+1][x-1]='/';
        matrix[y+1][x]='_';
        matrix[y+1][x+1]='_';
        matrix[y+1][x+2]='_';
        return;
    }
    // k层是用三个k-1层组成的
    dfs(y,x,k-1);// 递归最上层的三角形
    dfs(y+(1<<k-1),x-(1<<k-1),k-1);// 下一层左下角
    dfs(y+(1<<k-1),x+(1<<k-1),k+1);// 下一层右下角
}
void output()
{
    for(int y=1;y<=1<<n;++y)
    {
        for(int x=1;x<=1<<n;++x)
        {
            if(matrix[y][x])
            {
                cout<<matrix[y][x];
            }
            else
            {
                cout<<' ';
            }
        }
    }
}
int main()
{
    cin>>n;
    dfs(1,1<<n,n);// 从第1层，1<<n这个位置开始画，一共有n层
    output();
}