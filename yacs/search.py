import json
import os

path=os.path.dirname(os.path.abspath(__file__))
all_questions=[]
me=[]
with open(os.path.join(path, "test.json"), "r") as read_file:
    test_data=json.load(read_file)
    for i in test_data:
        all_questions.append(i["id"])
    all_questions.sort()
with open(os.path.join(path, "me.json"), "r") as read_file:
    me_data=json.load(read_file)
    for i in me_data:
        me.append(i)
    me.sort()
print(all_questions)
print(me)