import json
import os


path = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(path, 'problem_yacs.json'), 'r') as f:
    problem = json.load(f)
words = ""
words += problem["description"]+"\n\n"
if problem["background"] != "":
    words += "### 背景知识\n\n"+problem["background"]+"\n\n"
words += "### 输入格式\n\n"+problem["inputFormat"]+"\n\n"
words += "### 输出格式\n\n"+problem["outputFormat"]+"\n\n"
words += "### 数据范围\n\n"+problem["dataRange"]+"\n\n"

c = 1
for i in problem["exampleList"]:
    words += f"#### 样例输入{c}\n```cpp\n"+i["input"] + \
        f"\n```\n### 样例输出{c}\n```cpp\n"+i["output"]+"\n```\n"
    c += 1
    if "note" in i.keys() and i["note"] != "":
        words += "说明\n\n"+i["note"]+"\n\n"

with open(os.path.join(path, 'problem_yacs.md'), 'w', encoding="utf-8") as f:
    f.write(words)

yaml = ""
yaml += f"pid: YACS-{problem['id']}\n"
yaml += f"title: 【YACS】{problem['title']}\n"
if problem["knowledgePoint"] != "":
    yaml += f"tag: \n- {problem["knowledgePoint"]}\n"
with open(os.path.join(path, 'problem_yacs.yaml'), 'w', encoding="utf-8") as f:
    f.write(yaml)
